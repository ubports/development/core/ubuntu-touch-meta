To add a new framework for a major version or any other reasons:
- Add framework in ubuntu-touch-meta (this package).
- Add policy in apparmor-easyprof-ubuntu.
- Add mapping between framework -> policy_version in click-apparmor
- Add mapping in click-review-tool
  - lint:framework
  - security:policy_version_exists:appname.apparmor
  - security:policy_version_matches_framework:appname.apparmor
